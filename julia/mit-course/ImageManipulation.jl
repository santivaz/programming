using Images, KittyTerminalImages, TestImages
img = testimage("Chelsea")

# --------------------------------------------------
# Different modes of accessing an image
# --------------------------------------------------

# Simple. Outside of the image uses fallback
function simpleaccess(img, i, j, fallback=RGB(0,0,0))
    i = floor(Int, i)
    j = floor(Int, j)
    # if prod([1 1] .≤ [i j] .≤ size(img))
    if (1 ≤ i ≤ size(img)[1]) & (1 ≤ j ≤ size(img)[2])
        return img[i, j]
    else
        return fallback
    end
end

# Outside of the image uses the closest pixel inside of the image
function extendedborders(img, i, j)
    i = floor(Int, i)
    j = floor(Int, j)
    return img[min(size(img)[1], max(1, i)), min(size(img)[2], max(1,j))]
end

# Tiles the plane with the image
function mosaic(img, i, j)
    i = floor(Int, i)
    j = floor(Int, j)
    return img[mod1(i, size(img)[1]), mod1(j, size(img)[2])]
end

# Similar to mosaic but the image is reflected about each border
function reflect(img, i, j)
    i = floor(Int, i)
    j = floor(Int, j)
    # Get coordinates of the tile
    p = floor((i-1)/size(img)[1])
    q = floor((j-1)/size(img)[2])
    # This business with the 0.5 is because it is a reflection around x = 0.5 (resp y = 0.5) when p (resp q) is odd.
    return mosaic(img, 0.5 + (i-0.5)*(-1)^p, 0.5 + (j-0.5)*(-1)^q)
end
# --------------------------------------------------



# --------------------------------------------------
# Convolution
# - Kernels are arrays of real numbers and they are automatically centred
# --------------------------------------------------
function convolve(img, kernel, getpixel=extendedborders)
    h, w = size(img) # image size
    new_img = Array{RGB{Float16}}(undef, h, w) # we specify the type as the default does not have enough precision
    kh, kw = size(kernel) # kernel size
    kho, kwo = (kh+1) ÷ 2, (kw+1) ÷ 2 # centre of the kernel
    for i in 1:h, j in 1:w # iterate over image pixels
        new_img[i,j] = sum([kernel[u,v]*getpixel(img, i+u-kho, j+v-kwo) for u=1:kh, v=1:kw])
    end
    return new_img
end
# --------------------------------------------------



# --------------------------------------------------
# Kernels
# --------------------------------------------------
function uniformkernel(n)
    return [1/n^2 for i=1:n, j=1:n]
end
# --------------------------------------------------




# --------------------------------------------------
# Some colour manipulation stuff. I am sure that all of this is built in
# --------------------------------------------------
function intensity(color)
    return sum(float.([color.r color.g color.b]))
end

function normalize(color)
    return color/intensity(color)
end
# --------------------------------------------------




# --------------------------------------------------
# Finally realising my dream of applying a holomorphic function to an image
# --------------------------------------------------
function deform(img, in_scale, out_height, out_width, out_scale, fun, centre=true, mode=simpleaccess)
    # --------------------------------------------------
    # Both the input image and the output image are embedded in the complex place. The colour of the output
    # at z is given by the colour of the input at f(z).
    #
    # img = image to modify
    # in_scale = side of the pixels of img when embedded in the complex plane
    # out_height, out_width = resolution of the result
    # out_scale = same as in_scale but for the output
    # fun = function to apply
    # centred = whether to have the origin in the middle of the image (otherwise it is the top left corner)
    # mode = image accessing mode
    # --------------------------------------------------
    getpixel = mode
    
    # Find the pixel at the origin according to "centre""
    in_zero = (centre ? [size(img)[1] size(img)[2]]/2 : [0 0])
    out_zero = (centre ? [out_height out_width]/2 : [0 0])

    # Float16 as an output type is not supported later on to make animations.
    # output = Array{RGB{Float16}}(undef, out_height, out_width)
    output = Array{RGB{N0f8}}(undef, out_height, out_width)

    for x in 1:out_height, y in 1:out_width
        # iterate over output pixels
        z = dot([x y]-out_zero, [1 im])*out_scale # complex number corresponding to the pixel in the output
        w = fun(z) # complex number corresponding to the pixel of the input
        # pixel of the input
        u = real(w)/in_scale + in_zero[1]
        v = imag(w)/in_scale + in_zero[2]
        output[x, y] = getpixel(img, u, v)
    end
    return output
end

# shortcut for applying f(x) = x^n
    function twist(img, res, n, mode=simpleaccess)
    f(x) = x^n    
    in_scale = 2/sqrt(size(img)[1]^2 + size(img)[2]^2) # simplest way I could find of guarant
    out_scale = 2/res
    return deform(img, in_scale, res, res, out_scale, f, true, mode)
end

# Creates a stack of "total" images twisted from "min" to "max". The rate at which the twist increases is exponential. This way the edge always seems to move at the same speed.
# The idea is to then create a video out of it.
function twiststack(img, res, min, max, total, mode=simpleaccess)
    n(x) = min*(max/min)^(x/total) # twist of the x-th image
    stack = []
    for i in 1:total
        println(i,"  ", n(i)) # print progress
        push!(stack, twist(img, res, n(i), mode))
    end
    return stack
end

# same but linear rate
function lineartwiststack(img, res, min, max, total, mode=simpleaccess)
    n(x) = min + (max-min)*x/total # twist of the x-th image
    stack = []
    for i in 1:total
        println(i,"  ", n(i)) # print progress
        push!(stack, twist(img, res, n(i), mode))
    end
    return stack
end
# --------------------------------------------------
