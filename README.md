# General Code Projects

## What is this?
This is a **monorepo** containing many of my old coding projects, from high school to the present. Most of them are small, unremarkable scripts I did while learning, following a video, or trying to test some maths hypothesis. However, there are some more notable projects hidden in here.

## The future
To simplify things moving forward, big projects will have their own repo. And they may or may not be added here as a submodule (likely not).
