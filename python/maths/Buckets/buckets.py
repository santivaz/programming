def step(buckets):
    min_dif = buckets[0] + buckets[2] + buckets[1]
    for i in range(3):
        dif = buckets[i] - buckets[(i + 1) % 3]
        if dif >= 0 and dif <= min_dif:
            min_dif = dif
            best = i
    buckets[best] = min_dif
    buckets[(best + 1) % 3] = 2*buckets[(best + 1) % 3]
    return buckets


max = 100
for i in range(max):
    for j in range(max):
        for k in range(max):
            buckets = [i, j, k]
            original = [i, j, k]
            steps = 0
            step_limit = (i+j+k)**3
            while(not buckets[0]*buckets[1]*buckets[2] == 0):
                buckets = step(buckets)
                if steps > step_limit:
                    print("FAIL: ", original)
                    break
                steps += 1
