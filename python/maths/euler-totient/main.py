import matplotlib.pyplot as plt
from tqdm import tqdm


# none of these are mine
def phi(n):
    phi = 0
    for k in range(1, n+1):
        a, b = n, k
        while b:
            a, b = b, a % b
        if a == 1:
            phi += 1
    return phi

# if you stare at this for long enough you'll see how it works
def phi_interval(n):
    result = [*range(n+1)]
    for i in range(2, n+1):
        if result[i] == i:
            for j in range(i, n+1, i):
                result[j] -= result[j] // i
    return result



if __name__=="__main__":
    l = phi_interval(10000000)
    data = [l[i]/(i+1) for i in range(len(l))]
    plt.ecdf(data)
    plt.plot([0, 1], [0, 1])
    plt.show()



