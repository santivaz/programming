import math
import matplotlib.pyplot as plt
import numpy as np
from definitions import *

def build(D, N): 
    # Adds multiples of k starting with k=D and stops when quality is maximized
    kvals, Cvals = [], []
    s = {D} 
    max_quality = 0
    for k in range(D, N//2):
        s = s.union({k*l for l in range(int(N/k))})
        kvals.append(k)
        Q = quality(s, D)
        Cvals.append(Q*D/N)
        if max_quality < Q:
            print(k, "YES")
            max_quality = Q
            print(max_quality*D/N)
            best_s = s 
        else:
            print(k, "NO")
    plt.plot(kvals, Cvals)
    return best_s


def smart_build(D, N, s=set()):
    # Adds multiples of k whenever doing so improves the quality. It always takes the smallest value.
    X = set()
    kvals, Cvals = [], []
    if s == set():
        max_quality = 0
    else:
        max_quality = quality(s, D)
    k = D
    while k <= N/2:
        new_s = s.union({k*l for l in range(1,1+ int(N/k))})
        Q = quality(new_s, D)
        if max_quality < Q:
            kvals.append(k)
            Cvals.append(Q*D/N)
            print(k, "YES")
            X = X.union({k})
            max_quality = Q
            print(max_quality*D/N)
            s = new_s
            k = D
        else:
            k +=1
            # print(k, "NO")
    # plt.plot(kvals, Cvals)
    return s, X


if __name__ == "__main__":
    for D in range(24,25):
        Nvals, Cvals = [], []
        for N in range(3000, 3001):
            print("N =", N)
            # Dvals, Cvals = [], []
            Nvals.append(N)
            s, X = smart_build(D, N)
            C = (quality(s, D)+1)*D/N
            # Dvals.append(D)
            Cvals.append(C)
            print(C)
            print(sorted(X))
        # plt.plot(Dvals, Cvals)
        plt.plot(Nvals, Cvals, label=str(D))

    # Dvals = []
    # Cvals = []
    # dvals = []
    # nvals = []
    
    # plt.plot(Dvals, Cvals)
    # plt.plot(Dvals, dvals)
    # plt.plot(Dvals, nvals)
    plt.legend(loc="upper left")
    plt.show()
