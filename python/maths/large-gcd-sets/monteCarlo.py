# sort of giving up on explicit calculations for big D, and now trying to estimate it
from progressbar import ProgressBar
from tqdm import tqdm # for a progress bar
import math
import matplotlib.pyplot as plt
import numpy as np
import random
from definitions import *

def hasDivs(S, x): # returns divisors of the int x in the set S
    return {s for s in S if x % s == 0}

def mc(D, S, K = 10000):
    n = 0
    n_S = 0
    n_no_S = 0
    for k in tqdm(range(K)): #tqdm for a progress bar
        a = random.getrandbits(256)
        b = random.getrandbits(256)
        if not ( not hasDivs(S, a) or not hasDivs(S, b)):  # Check if its a valid pair
            n += 1
            d = math.gcd(a, b)
            if d >= D: # check if gcd is large
                if not hasDivs(S, d): 
                    n_no_S += 1 # if the gcd is not a mult of something in S, add 1 to the random part
                else:
                    n_S += 1 # else add 1 to the deterministic part
    return n, n_S, n_no_S

def getconfidence(n, N, c = 1.96):
    # N trials and n successes. We check confidence that true ration n/N is in a confidence interval given by c
    # default value of c=1.96 corresponds to a 95% confidence interval
    # ERROR: sometimes this gives a division by 0 error :(
    if N < 2: return 1
    var = n*(N-n)/(N*(N-1)) # estimator for the variance
    return c*(var/N)**(1/2)

def mc_dynamic(D, S, e = 0.01, c = 1.96):
    # dynamically stop when with prob e, we are in the confidence interval given by c
    n = 0 # num of pairs in our set so far
    n_S = 0 # deterministic part
    n_no_S = 0 # random part
    K = 0 # total num of steps so far
    I = 10000 # step size
    bar = ProgressBar(max_value=100)
    while True:
        for i in range(I): # we execute I steps at a time
            K += 1
            a = random.getrandbits(256)
            b = random.getrandbits(256)
            if not ( not hasDivs(S, a) or not hasDivs(S, b)):  # Check if its a valid pair i.e. they have divs in S
                n += 1
                d = math.gcd(a, b)
                if d >= D: # check if gcd is large
                    if not hasDivs(S, d): 
                        n_no_S += 1 # if the gcd is not a mult of something in S, add 1 to the random part
                    else:
                        n_S += 1 # else add 1 to the deterministic part
        # we now check the confidence
        # get error for n_S+n_no_S wrt n
        e1 = getconfidence(n_S + n_no_S, n, c)
        # get error for n wrt K
        e2 = getconfidence(n, K, c)
        # print(f"K  = {K}")
        # print(f"e1 = {e1}")
        # print(f"e2 = {e2}")
        # if n > 0:
        #     print("Deterministic part: ", D*(n_S)/(K*n)**(1/2))
        #     print("Random part:        ", D*(n_no_S)/(K*n)**(1/2))
        #     print("Total:              ", D*(n_S + n_no_S)/(K*n)**(1/2))
        # print("#"*20)
        # print((e1/e)**(-2))
        bar.update(min(100, int(100*(e1/e)**(-2))))
        if e1 < e and e2 < e: return n, n_S, n_no_S, K
            


if __name__ == "__main__":
    D = 2
    S = set(range(D, 18*D//12+1))
    K = 10**7
    print("D: ", D)
    n, n_S, n_no_S = mc(D, S, K)
    print("Deterministic part: ", D*(n_S)/(K*n)**(1/2))
    print("Random part:        ", D*(n_no_S)/(K*n)**(1/2))
    print("Total:              ", D*(n_S + n_no_S)/(K*n)**(1/2))
    # print(n, n_S, n_no_S)
