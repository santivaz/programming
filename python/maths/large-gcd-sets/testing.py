import math
from definitions import *

if __name__ == "__main__":
    D = 40
    k = D//2 +1
    S = set(range(D, D+k))
    print(eval_mc_dynamic(D, S, 0.001))

    def ieSums(S, depth): # inclusion exclusion sum to depth d with exp -s
        subsets = [set()]
        for d in range(1, max(depth) + 1):
            old_subsets = subsets.copy()
            for sub in old_subsets:
                # print(sub)
                subsets += [sub.union({elt}) for elt in S if sub.union({elt}) not in subsets]
        sum1, sum2 = 0 , 0
        for sub in subsets:
            if 1 <= len(sub) <= depth[0]:
                sum1 += (-1)**(len(sub))*(math.lcm(*sub))**(-1)
            if 1 <= len(sub) <= depth[1]:
                sum2 += (-1)**(len(sub))*(math.lcm(*sub))**(-2)
                
        return sum1, sum2

    # depth = len(S)
    depth = [5, 6]
    sum1, sum2 = ieSums(S, depth)
    print(f"To depth {depth}, the determined part is {D*sum2/sum1}")

