import math
import matplotlib.pyplot as plt
import numpy as np
from definitions import *


def prune(s, D): # remove vertices with low deg all at the same time
    s_bad = {x for x in s if d(s, D, x) < e(s, D)/len(s)} # edges with deg < half the average deg
    return s - s_bad

if __name__ == "__main__":
    N = 500
    s = {1+n for n in range(N)}
    Dvals = []
    Cvals = []
    C_or_vals = []
    dvals = []
    nvals = []
    for D in range(2, int(math.sqrt(N))):
        D_delta_original = delta(s, D)
        C_original = D*(D_delta_original*len(s)+1)/N 
        C_or_vals.append(C_original)
        pruned = prune(s, D)
        n = len(pruned)
        D_delta = delta(pruned, D)
        C = D*(D_delta*len(pruned)+1)/N
        Dvals.append(D)
        Cvals.append(C)
        dvals.append(D_delta)
        nvals.append(n/N)
        print(D, C)
    
    plt.plot(Dvals, Cvals)
    plt.plot(Dvals, C_or_vals)
    # plt.plot(Dvals, dvals)
    # plt.plot(Dvals, nvals)
    plt.show()

