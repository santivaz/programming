from tqdm import tqdm # for a progress bar
import math
from monteCarlo import *

E = lambda s, D, a: {b for b in s if b != a and math.gcd(a,b) >= D} # set of neighbours of a

d = lambda s, D, a: len(E(s, D, a)) # degree of a

def e(s, D): # number of edges
    x = 0
    for a in tqdm(s) : x += d(s, D, a)
    return x//2

def delta(s, D): # edge density (times 2)
    return 2*e(s, D)/(len(s)**2)

def quality(s, D): # delta*size
    return 2*e(s, D)/len(s)

def multiples(S, N): # get multiples of elts of S up to N
    A = set()
    for k in S:
        A = A.union({k*j for j in range(1, 1 + int(N/k))})
    return A

def eval(D, S , N):
    A = multiples(S, N)
    return (1+quality(A, D))*D/N

def eval_mc(D, S, K = 10**6):
    n, n_S, n_no_S = mc(D, S, K)
    return D*(n_S + n_no_S)/(K*n)**(1/2)

def eval_mc_dynamic(D, S, e=0.001, c=1.96): # im not quite sure how to interpret e and c in my case. I could just have a coeff as c/e is what matters
    # idea: add an estimate for how many steps are left
    n, n_S, n_no_S, K = mc_dynamic(D, S, e, c)
    print("Deterministic part: ", D*(n_S)/(K*n)**(1/2))
    return D*(n_S + n_no_S)/(K*n)**(1/2)
