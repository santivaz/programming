# import numpy as np
# import matplotlib
import math
import seaborn as sns
import matplotlib.pyplot as plt
# import pandas as pd
y = 10
k = 3
def listProd(l1, l2):
    r = []
    for a in l1:
        for b in l2:
            r.append(a*b)
    return r

def f(y, k):
    l = [1]
    ran = range(y,2*y)
    for i in range(k):
        l = listProd(l, ran)
    return l

def sq_sum(S):
    sum = 0
    for x in S: sum += x**(-2);
    return sum

def lcm_sum(S):
    sum = 0;
    for x in S:
        for y in S:
            if x<y:
                sum += math.lcm(x, y)**(-2)
    return sum

def lcm_sum_dic(S):
    sum = dict();
    for x in S:
        sum[x] = 0
        for y in S:
            if x<y:
                sum[x] += (math.gcd(x,y)/y)**2
    return sum


if __name__ == "__main__":
    y = 21
    k = 2
    # S = list(set(f(y,k)))
    S = list(set(f(y,k)))
    # print("sq_sum     =", sq_sum(S))
    # print("lcm_sum    =", lcm_sum(S))
    data = lcm_sum_dic(S)
    max_factor = max(list(set(data.values())))
    print("max factor = ", max_factor)
    print("attained by ", list(data.keys())[list(data.values()).index(max_factor)])
    sns.displot(list(data.values()), kde=True)
    # plt.scatter(list(data.keys()), list(data.values()), )
    # plt.hist(S, bins = 15)
    # plt.hist(f(y,k), bins = 15)
    sns.displot(S, kde=True, rug=True)
    plt.show()
