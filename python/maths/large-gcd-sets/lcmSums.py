import math
from progressbar import ProgressBar
from definitions import *

def ieSums(S, depth): # inclusion exclusion sum to depth d with exp -s
    subsets = [set()]
    for d in range(1, max(depth) + 1):
        old_subsets = subsets.copy()
        for sub in old_subsets:
            # print(sub)
            subsets += [sub.union({elt}) for elt in S if sub.union({elt}) not in subsets]
    sum1, sum2 = 0 , 0
    for sub in subsets:
        if 1 <= len(sub) <= depth[0]:
            sum1 += (-1)**(len(sub))*(math.lcm(*sub))**(-1)
        if 1 <= len(sub) <= depth[1]:
            sum2 += (-1)**(len(sub))*(math.lcm(*sub))**(-2)
    return sum1, sum2


