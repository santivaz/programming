# Just testing different ways of generating the sequence of numbers with even depth

from time import time

def get_depth_parity(N):
    # storing every value it touches without recursion
    dp = {i:-1 for i in range(1, N+1)}
    dp[1] = 0
    for n in range(1, N+1):
        # print(n)
        if dp[n] == -1:
            part = [[], []]
            p = 0

            while n not in dp or dp[n] == -1:
                part[p].append(n)
                if n%2 == 0:
                    n = n//2
                else:
                    n = 3*n + 1
                    p = 1 - p

                    part[p].append(n)
                    n = n//2

            v = dp[n]
            for m in part[p]:
                dp[m] = v
            v = 1 - v
            for m in part[1-p]:
                dp[m] = v

    return list(dp.values())[:N]

def collatz_travel(n, f, stop):
    if not stop(n)[0]:
        if n%2 == 0:
            return f(n, collatz_travel(n//2, f, stop))
        else:
            return f(n, collatz_travel(3*n + 1, f, stop))
    else:
        return stop(n)[1]

def get_depth_parity_rec(N):
    # storing every value with recursion
    dp = {1:0}
    def stop(n):
        if n in dp.keys():
            return True, dp[n]
        else:
            return False, 0
    def set_parity(n, prev):
        if n%2 == 0:
            par = prev
        else:
            par = 1-prev

        dp[n] = par 
        return par

    for k in range(1, N+1):
        collatz_travel(k, set_parity, stop)
    return dp
    
def generate_de(N):
    # not storing every value

    de = []

    if len(de) == 0:
        de.append(True)

    for k in range(len(de) + 1, N):
        d = 0
        while k > len(de):
            if k%2 == 0:
                k = k//2
            else:
                k = (3*k+1)//2
                d += 1
        de.append(de[k-1] == (d%2 == 0))
    return de


if __name__=="__main__":
    N = 10**7
    st = time()
    get_depth_parity(N)
    end = time()
    print(end-st)

    st = time()
    generate_de(N)
    end = time()
    print(end-st) # so this is the fastest by a very considerable margin and I really cannot understand why :(

    st = time()
    get_depth_parity_rec(N)
    end = time()
    print(end-st)
