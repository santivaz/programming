import json
import matplotlib.pyplot as plt
import math
from tqdm import tqdm
import numpy as np

def load_sequence(file_path = "the-sequence.txt"):
    with open(file_path, "r") as file:
        seq = json.load(file)
    return seq

def generate_de(N):

    de = []

    if len(de) == 0:
        de.append(True)

    for k in tqdm(range(len(de) + 1, N), ascii=" >-"):
        d = 0
        while k > len(de):
            if k%2 == 0:
                k = k//2
            else:
                k = (3*k+1)//2
                d += 1
        de.append(de[k-1] == (d%2 == 0))
    return de

def save_bool_data(bool_data, file_path = 'data.bin'):
    print(f"Saving data to {file_path}")
    np_data = np.array(bool_data, dtype=bool)
    packed_data = np.packbits(np_data)
    packed_data.tofile(file_path)
    print("Done!")

def load_bool_data(file_path = 'data.bin'):
    # print("Loading data ...")
    loaded_packed_data = np.fromfile(file_path, dtype=np.uint8)
    return np.unpackbits(loaded_packed_data).astype(bool)

def exp_transform(bool_data, N = 10000):
    # get a list of the indices of the q**i th True value in bool_data.
    # q is taken so that there are (potentially) N points.
    q = len(bool_data)**(1/N)

    def get_next(n, k):
        # find m such that there are k numbers x with even depth and n<x<=m.
        count = 0
        for i in range(n, len(bool_data)):
            if bool_data[i]:
                count +=1
            if count == k:
                return i+1
        return -1 # return -1 if it reaches the end

    seq = [1] # i know im assuming that bool_data[0] but oh boy icba to generalise
    actual_pos = 1
    for i in range(1, N):
        print(int(q**i))
        k = int(q**i - actual_pos)
        actual_pos = k + actual_pos # since q**i is likely not an integer, we keep track of the actual position of the number we find.
        if k == 0:
            seq.append(seq[-1])
        else:
            next = get_next(seq[-1], k)
            if next == -1:
                break # q**i is bigger than the total number of points in the data.
            else:
                seq.append(next)

    return seq, q
 

if __name__=="__main__":
    K = 8
    file_path = f'de-bool-1e{K}.bin'

    de_bool = generate_de(10**K)
    save_bool_data(de_bool, file_path)


    # de_bool = load_bool_data(file_path)
    # seq, q = exp_transform(de_bool)
    #
    # fig, ax = plt.subplots()
    # ax.plot([seq[i]/q**i-2 for i in range(len(seq))])
    # plt.show()

