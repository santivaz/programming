import matplotlib.pyplot as plt
from tqdm import tqdm
# import numpy as np
from bitarray import bitarray
from random import randint
from math import sin, cos, log, log10, pi

def get_parities(N):
    depth_parity = bitarray(N)
    depth_parity[0] = 0 # set 1

    for n in tqdm(range(2, N+1), ascii=" >-"):
        d = 0 # depth of n
        k = n
        while k >= n:
            if k%2 == 0:
                k = k//2
            else:
                k = (3*k+1)//2
                d += 1
        depth_parity[n-1] = ( depth_parity[k-1] != d%2 )

    return depth_parity

def save_bits(bits, file_path = 'data.bin'):
    with open(file_path, 'wb') as f:
        bits.tofile(f)

def load_bits(file_path = 'data.bin'):
    bits = bitarray()
    with open(file_path, 'rb') as f:
        bits.fromfile(f)
    return bits

def get_d_e(n):
    d, e = 0, 0
    while n > 1:
        if n%2 == 0:
            n = n//2
            e += 1
        else:
            n = (3*n+1)//2
            d += 1
            e += 1
    return d, e

def test(bits, T):
    L = len(bits)
    for t in range(T):
        n = randint(1, L)
        d, e = get_d_e(n)
        if d%2 != bits[n-1]:
            print("Failed: ", n)
            print("Test: ", t)
            return False
    print("Test passed!")
    return True

def compare(fX, gX):
    se = 0
    for fx, gx in zip(fX, gX):
        se += (fx-gx)**2
    return se/len(fX)

def eval_parameters(a,b,c, X, fX_normalised):
    g = lambda x: x/2*(1-1/(a*log(x))*sin(pi*2*b*log(x)+c))
    # g = lambda x: x/2*(1-sin(pi*x)**2/(pi*x)**2-1/(a*log(x))*sin(pi*2*b*log(x)+c))
    # g = lambda x: x/2*(1+sin(0.16*pi*log(x))**4/(0.1*pi*log(x))**4)
    g_vec = lambda X: [g(x) for x in X]
    gX = g_vec(X)
    gX_normalised = [2*gx/x for x, gx in zip(X, gX)]
    return compare(fX_normalised, gX_normalised)



    



if __name__=="__main__":
    K = 10
    file_path = f'data/depthParity-1e{K}.bin'

    ################### 

    # depth_parity = get_parities(10**K)
    # print(f"Saving data to {file_path}")
    # save_bits(depth_parity, file_path)
    # print("Done!")

    # print("Testing ...")
    # test(depth_parity, 10000)

    ###################

    depth_parity = load_bits(file_path)

    # even depth counting functions
    # f = lambda x: depth_parity.count(False, 0, int(x))
    f = lambda y, z: depth_parity.count(False, int(y), int(z))
    def f_vec(X):
        # Instead of evaluating f(x) for x in X, which does a lot of double counting, we count the values between the xs and then add together with a total cumulative sum. The thing below does this in a very nice way: total is the cumulative sum, y keeps track of the previous value.
        y, total = 0, 0
        fX = [total := total + f(y, y := z) for z in X]
        return fX


    # k = 100000 # the step
    # X = list(range(k, len(depth_parity), k)) # sample at every k. Start at k to avoid noise near 0.

    k = 1000000
    l = len(depth_parity)
    cutoff = 10**1
    X = [x for i in range(1, k+1) if (x := l**(i/k)) > cutoff]
    fX = f_vec(X)



    ###################
    # comparisons

    fX_normalised = [2*fx/x for x, fx in zip(X, fX)]

    # a = 20/log(10)-0.46
    # b = 20/log(10)
    # c= 1.181649 #The last 2 digits might not be optimal

#     min_mse = 1
#     for i in tqdm(range(-50, 50)):
#         for j in range(-50,50):
#             # for k in range(-50, 50):
#             x = a + i/100
#             y = b + j/100
#             # z = c + k/10000000
#             mse = eval_parameters(x,y,c, X, fX_normalised)
#             if mse < min_mse:
#                 min_mse = mse
#                 print("New minimum:")
#                 print(x)
#                 print(y)
#                 # print(z)



    

    ###################
    # Some plots
     
    fig, ax = plt.subplots()
    # ax.plot(X, [fx-x/2 for x, fx in zip(X, fX)])
    # ax.plot(X, [2*fx/x for x, fx in zip(X, fX)])
    ax.plot(list(range(len(X))), fX_normalised, label='Real deal')
    # ax.scatter(periods, [1]*len(periods), color='green', marker='D')
    # plt.show(block=False)

    a = 20/log(10)-0.46
    b = 20/log(10)
    c= 1.181649 #The last 2 digits might not be optimal
    # h = lambda x: log(x/10)
    # A = a*log(10**10)/h(10**10)
    # g = lambda x: x/2*(1-1/(A*h(x))*sin(pi*2*b*log(x)+c))

    # g = lambda x: x/2*(1-sin(pi*x)**2/(pi*x)**2-1/(a*log(x))*sin(pi*2*b*log(x)+c))
    # g = lambda x: x/2*(1+sin(0.16*pi*log(x))**4/(0.1*pi*log(x))**4)

    # h1 = lambda x: 40*sin(2*pi*0.14*x+pi*3/4)/(x**4)
    h3 = lambda x: sin(2*pi*b*x+c)/(a*x)
    g = lambda x: x/2*(1-h3(log(x)))

    g_vec = lambda X: [g(x) for x in X]
    gX = g_vec(X)
    gX_normalised = [2*gx/x for x, gx in zip(X, gX)]


    ax.plot(list(range(len(X))), gX_normalised, label='Model', alpha=0.5)
    # ax.plot(list(range(len(X))), [1-h3(log(x)) for x in X], label='Model', alpha=0.5)
    # ax.plot(list(range(len(X))), [1-h1(log(x)) for x in X], label='Model', alpha=0.5)
    plt.legend()
    plt.show(block=False)

    # ax.plot([i for i in range(len(X)) if (gX_normalised[i] > 1.004 or gX_normalised[i] <0.996)], [(f-1)/(g-1) for f,g in zip(fX_normalised, gX_normalised) if (g > 1.004 or g <0.996)] )
    ax.plot(list(range(len(X))), [1+f-g for f,g in zip(fX_normalised, gX_normalised)] )
    # plt.show()
    ###################




    
    ####################
    ## period length analysis

    #y = 2*fX[0]/X[0]-1
    #periods = [i*K/k for i in range(len(fX)) if y*(y:=2*fX[i]/X[i]-1) < 0 and y > 0  ] # notice the normalization. This way the period is as a log in base 10.

    ## # length of each period
    ## prev = periods[0]
    ## lengths = [-prev + (prev:=p) for p in periods[1:]]

    ## length of each period, but we remove the ones that are clearly due to noise of the sequence oscillating when it crossed the axis
    #prev = periods[0]
    #filtered_lengths = [l for p in periods[1:] if (l := -prev + (prev:=p)) > 1/30]


    #fig, ax = plt.subplots()
    #ax.plot(filtered_lengths, label='Ratio of period lengths (log_10)')

    #N = 20
    #ax.plot(
    #        list(range(N, len(filtered_lengths))),
    #        [sum(filtered_lengths[i-N:i])/N for i in range(N, len(filtered_lengths))],
    #        label=f'Moving average ({N})'
    #        )
    #N = 40
    #ax.plot(
    #        list(range(N, len(filtered_lengths))),
    #        [sum(filtered_lengths[i-N:i])/N for i in range(N, len(filtered_lengths))],
    #        label=f'Moving average ({N})'
    #        )
    #N = 60
    #ax.plot(
    #        list(range(N, len(filtered_lengths))),
    #        [sum(filtered_lengths[i-N:i])/N for i in range(N, len(filtered_lengths))],
    #        label=f'Moving average ({N})'
    #        )
    #N = 80
    #ax.plot(
    #        list(range(N, len(filtered_lengths))),
    #        [sum(filtered_lengths[i-N:i])/N for i in range(N, len(filtered_lengths))],
    #        label=f'Moving average ({N})'
    #        )


    #plt.legend()
    #plt.show(block=False)
    ####################




    plt.show()
