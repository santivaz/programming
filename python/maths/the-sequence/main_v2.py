import matplotlib.pyplot as plt
from tqdm import tqdm
# import numpy as np
from bitarray import bitarray
from random import randint
import math

def get_parities(N):
    depth_parity = bitarray(N)
    depth_parity[0] = 0 # set 1

    for n in tqdm(range(2, N+1), ascii=" >-"):
        d = 0 # depth of n
        k = n
        while k >= n:
            if k%2 == 0:
                k = k//2
            else:
                k = (3*k+1)//2
                d += 1
        depth_parity[n-1] = ( depth_parity[k-1] != d%2 )

    return depth_parity

def save_bits(bits, file_path = 'data.bin'):
    with open(file_path, 'wb') as f:
        bits.tofile(f)

def load_bits(file_path = 'data.bin'):
    bits = bitarray()
    with open(file_path, 'rb') as f:
        bits.fromfile(f)
    return bits

def get_d_e(n):
    d, e = 0, 0
    while n > 1:
        if n%2 == 0:
            n = n//2
            e += 1
        else:
            n = (3*n+1)//2
            d += 1
            e += 1
    return d, e

def test(bits, T):
    L = len(bits)
    for t in range(T):
        n = randint(1, L)
        d, e = get_d_e(n)
        if d%2 != bits[n-1]:
            print("Failed: ", n)
            print("Test: ", t)
            return False
    print("Passed!")
    return True

def get_from_positions(bits, index):
    partial_counts = []
    




    seq = []
    count = 0
    i = 0
    for n in tqdm(range(1, len(bits)+1), ascii=" >-"):
        if bits[n-1] == 0:
            count += 1
        while count == index[i]:
            seq.append(n)
            i += 1
            if i == len(index):
                return seq
    return seq

def exp_transform(bits, q):
    # get a list of the indices of the q**i th zero value in bits.
    N = int( math.log(len(bits)) / math.log(q) )
    index = [int(q**i) for i in range(1, N)]
    return get_from_positions(bits, index)


if __name__=="__main__":
    K = 10
    file_path = f'depthParity-1e{K}.bin'

    # depth_parity = get_parities(10**K)
    # print(f"Saving data to {file_path}")
    # save_bits(depth_parity, file_path)
    # print("Done!")

    # print("Testing ...")
    # test(depth_parity, 10000)

    print("Loading data ...")
    depth_parity = load_bits(file_path)
    print("Loaded!")

    # print("Transforming data ...")
    # q = len(depth_parity)**(1/100000)
    # seq = exp_transform(depth_parity, q)
    #
    # fig, ax = plt.subplots()
    # ax.plot([seq[i]/q**(i+1)-2 for i in range(len(seq))])
    # plt.show()

    # print(depth_parity.count(False))
    L= 10000000
    seq = []
    for i in range(1, int(len(depth_parity)/L)):
        # start = L*i
        # end = start + L
        seq.append(depth_parity.count(False, 0, i*L)*2/(L*i)- 1)
    fig, ax = plt.subplots()
    ax.plot(seq)
    plt.show()









