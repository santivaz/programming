# x^y - y^x = c, x < y

import math

MAX_c = 10000000


def evaluate(x, y):
    return x**y - y**x


def initial_bound(c):
    return 3+math.floor(math.log(c)/2)


def solve_y(x, c):
    y = x+1
    while evaluate(x, y) < c:
        y += 1
    else:
        if evaluate(x, y) == c:
            print(c, x, y)


def solve(c):
    for x in range(2, initial_bound(c)):
        solve_y(x, c)


for c in range(1, MAX_c):
    solve(c)
