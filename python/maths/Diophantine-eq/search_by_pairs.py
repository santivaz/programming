def evaluate(x, y): return x**y - y**x


evaluations = {}

MAX = 10000
# dies at 3347

for y in range(2, 1 + MAX):
    print(y)
    for x in range(2, y):
        c = evaluate(x, y)
        evaluations[c] = evaluations.setdefault(c, 0) + 1
        if evaluations[c] > 1:
            print("EUREKA:")
            print(c, x, y)
