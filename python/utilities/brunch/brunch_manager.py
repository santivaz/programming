#!/usr/bin/python

# This is a small program to automatically book brunch the folowing sunday.
# Last edited: 12/05/2022

from selenium import webdriver
from selenium.webdriver.common.by import By

from datetime import datetime, timedelta

import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify

# launch headless firefox
fireFoxOptions = webdriver.FirefoxOptions()
fireFoxOptions.headless = True
driver = webdriver.Firefox(service_log_path="/tmp/geckodriver.log",
                           options=fireFoxOptions)

try:
    driver.get("https://meals.linc.ox.ac.uk/")

    usr = "2377255"
    pswrd = "VAZQUEZSAEZ"

    # enter username
    driver.find_element(By.ID,
                        "ContentPlaceHolder1_Login1_UserName").send_keys(usr)

    # enter password
    driver.find_element(By.ID,
                        "ContentPlaceHolder1_Login1_Password").send_keys(pswrd)

    # log in
    driver.find_element(By.ID,
                        "ContentPlaceHolder1_Login1_LoginButton").click()

    # find next sunday
    sunday = datetime.today()+timedelta(days=6-datetime.today().weekday())
    sunday_css = ('#ContentPlaceHolder1_calMealDates a[title="'
                  + sunday.strftime('%d %B') + '"]')
    driver.find_element(By.CSS_SELECTOR, sunday_css).click()

    # find brunch
    driver.find_element(By.XPATH,
                        "//option[contains(text(), 'Lunch - STU Brunch')]"
                        ).click()

    # book and confirm
    driver.find_element(By.CSS_SELECTOR,
                        '#ContentPlaceHolder1_btnBook').click()
    driver.find_element(By.CSS_SELECTOR,
                        '#ContentPlaceHolder1_btnBook').click()

    notify_text = "Brunch booked for "+sunday.strftime('%d %B')+"."

except Exception:
    notify_text = "Could not book brunch."

finally:
    driver.quit()
    Notify.init("BrunchManager")
    Notify.Notification.new('Brunch Manager', notify_text).show()
