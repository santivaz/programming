import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# the book really seems to want to use this style, but it doesn't work and they link to a 2016 site...
# plt.style.use('seaborn')
plt.style.use('dark_background')
mpl.rcParams['font.family'] = 'serif'

# np.random.seed(1000)
y = np.random.standard_normal(20)
x = np.arange(len(y))
plt.plot(x,y)
plt.plot(y.cumsum())
plt.show()

y = np.random.standard_normal((20, 2)).cumsum(axis=0)
y[:, 0] = y[:, 0]*100
plt.figure(figsize=(10, 6))
plt.plot(y[:, 0], lw=1.5, label='1st')  
plt.plot(y[:, 1], lw=1.5, label='2nd')  
plt.plot(y, 'ro')
plt.legend(loc=0)  
plt.xlabel('index')
plt.ylabel('value')
plt.title('A Simple Plot');
plt.show()
