import pandas as pd
import numpy as np

a = np.random.standard_normal((9, 4))
print(a)

df = pd.DataFrame(a)
# print(df)

df.columns = ['No1', 'No2', 'No3', 'No4']
# print(df)

dates = pd.date_range('2019-1-1', periods=9, freq='M')
# print(dates)

df.index = dates
print(df)

df.info()
df.describe()

np.log(df)


# this doesnt work idk what the author was smoking
# import matplotlib as mpl
# import matplotlib.pyplot as plt
# plt.style.use('seaborn')  
# mpl.rcParams['font.family'] = 'serif'  
# %matplotlib inline # what is this doing here? idk
# df.cumsum().plot(lw=2.0, figsize=(10, 6));

# ChatGPT gave me an exercise
# Generate a NumPy array of shape (3, 4) containing random integers between 1 and 10.
a = np.random.randint(1,10,(3,4))
# Create a Pandas dataframe from this NumPy array with column names ['A', 'B', 'C', 'D'] and index labels ['X', 'Y', 'Z'].
df = pd.DataFrame(a, columns=['A', 'B', 'C', 'D'], index=['X', 'Y', 'Z'])
# Add a new column 'E' to the dataframe which is the sum of columns 'A' and 'B'.
df['E'] = df['A'] + df['B']
# Filter the dataframe to show only rows where the value in column 'C' is greater than 5.
df = df[df['C'] > 5]
# Calculate the mean value of each column in the filtered dataframe.
df.mean()



# ChatGPT gave me another exercise
# Create a NumPy array of shape (5, 3) containing random floating-point numbers between 0 and 1.
a = np.random.uniform(0, 1, (5,3))
# Create a Pandas dataframe from this NumPy array with column names ['X', 'Y', 'Z'] and index labels ['A', 'B', 'C', 'D', 'E'].
df = pd.DataFrame(a, columns = ['X', 'Y', 'Z'], index =  ['A', 'B', 'C', 'D', 'E'])
# Add a new column 'Sum' to the dataframe which contains the sum of values in columns 'X', 'Y', and 'Z' for each row.
df['Sum'] = df.sum(axis=1)
# Filter the dataframe to show only rows where the value in column 'Sum' is greater than 1.
df_filtered = df.query('Sum > 1')
# Calculate the mean value of each column in the filtered dataframe.
df_filtered.mean()
