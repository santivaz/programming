# import re
# from io import StringIO
from datetime import datetime, timedelta
# import requests
import pandas as pd
import numpy as np
import yfinance as yf
from pandas_datareader import data as pdr
from os.path import exists



# Get list of stocks in the S&P 500
stocks = pd.read_csv('data/sp500_tickers.csv')

# Download market date for these stocks on the given period
yf.pdr_override() # make sure yf format is the same as pandas_datareader

START_DATE = '2022-01-01'
END_DATE = '2024-01-01'
DATA_FILE = f'data/closing_{START_DATE}_{END_DATE}.csv'

# Prevent downloading multiple times
if not exists(DATA_FILE):
    print(f'Downloading data from {START_DATE} until {END_DATE}')
    data = pdr.get_data_yahoo( list(stocks['Ticker']) , start = START_DATE, end = END_DATE)
    data['Close'].to_csv(DATA_FILE)

data = pd.read_csv(DATA_FILE, index_col='Date', parse_dates=True).dropna(axis=1)
data.head()

# purge stocks to only reflect things for which we have data
stocks = stocks.loc[stocks['Ticker'].isin(data.columns)]

##########
def eval_weights(annual_returns, cov_annual, weights):
    tickers = list(annual_returns.index)

    # Initialize empty lists
    portfolio_returns = []
    portfolio_volatility = []
    sharpe_ratio = []

    for weight in weights:
        returns = (np.dot(weight, (annual_returns)))# should it not use the geometric one rather than the arithmetic??
        volatility = np.sqrt(np.dot(weight.T, np.dot(cov_annual, weight)))

        sharpe = ((returns-1) / volatility)

        # Append the results
        sharpe_ratio.append(sharpe)
        portfolio_returns.append(returns-1)
        portfolio_volatility.append(volatility)

    # make a DataFrame with the results
    portfolio = {'Returns': portfolio_returns,
                 'Volatility': portfolio_volatility,
                 'Sharpe Ratio': sharpe_ratio}
    for counter, symbol in enumerate(tickers):
        portfolio[symbol+' Weight'] = [Weight[counter] for Weight in weights]
    df = pd.DataFrame(portfolio)

    return df

def get_portfolios_monte_carlo(tickers, num_portfolios):

    # Testing data. We are not really using this anyway
    dT = data.iloc[-100:,:][tickers]

    # Train data
    d = data.iloc[:-100, :][tickers]

    daily_returns = d.pct_change()
    annual_returns = (daily_returns.mean()*250)+1 # mentioned below, but I think I whould be using the geometric mean here?

    cov_daily = daily_returns.cov()
    cov_annual = cov_daily*250

    # Get list of weights
    num_assets = len(tickers)
    stock_weights = []
    for i in range(num_portfolios):
        # randomly select weights
        weights = np.random.random(num_assets)
        weights /= np.sum(weights)
        stock_weights.append(weights)

    return eval_weights(annual_returns, cov_annual, stock_weights)


def get_optimal_weights(annual_returns, cov_annual):
    n = len(annual_returns)

    # expand the cov matrix with ones except for a zero on the corner
    M = cov_annual.to_numpy()
    M = np.append(M, [[1 for i in range(n)]], 0)
    M = np.append(M, [[1] for i in range(n +1)], 1)
    M[n,n] = 0

    # This will be used to get the constants for the equations
    u = annual_returns.to_numpy()/2
    u = np.append(u, [0])

    weights_list = []
    # number of steps
    N = 100
    for i in range(N):
        # [0, 1] is broken down into N points
        # This is the parameter for this set of weights
        l = i/N
        # constants for the system of equations
        v = l*u
        v[n] = 1
        # solve system
        x = np.linalg.inv(M)@v
        # discard dummy variable
        weights_list.append(x[:-1])

    return weights_list

def get_portfolios(tickers):
    # Testing data. We are not really using this anyway
    dT = data.iloc[-100:,:][tickers]

    # Train data
    d = data.iloc[:-100, :][tickers]

    daily_returns = d.pct_change()
    annual_returns = (daily_returns.mean()*250)+1 # mentioned below, but I think I whould be using the geometric mean here?

    cov_daily = daily_returns.cov()
    cov_annual = cov_daily*250

    weights = get_optimal_weights(annual_returns, cov_annual)
    return weights
    # return eval_weights(annual_returns, cov_annual, weights)


#########
import matplotlib.pyplot as plt

tickers = [s for s in stocks['Ticker'] if s in data.columns][:8]
portfolios = get_portfolios_monte_carlo(tickers, 1000000)
portfolios.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio',
                 cmap='RdYlGn', edgecolors='black', grid=True)

#-------------------------------
sectors = []
for sec in stocks.iloc[:50].Sector:
    if sec not in sectors:
        sectors.append(sec)
sectors

tickers = [stocks.loc[stocks['Sector'] == sec].iloc[0]['Ticker'] for sec in sectors]
tickers[:1]


portfolios = get_portfolios_monte_carlo(tickers[:7], 100000)
portfolios.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio',
                 cmap='RdYlGn', edgecolors='black', grid=True)


portfolios_random = get_portfolios_monte_carlo(tickers, 100000)
portfolios_optimal = get_portfolios(tickers)

# portfolios = pd.concat([portfolios_random, portfolios_optimal])
# portfolios.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio', cmap='RdYlGn', edgecolors='black', grid=True)
portfolios_optimal.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio', cmap='RdYlGn', edgecolors='black', grid=True)
#--------------------------------
