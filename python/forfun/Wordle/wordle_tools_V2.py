def compare(secret, guess):
    comp = [0, 0, 0, 0, 0]
    for i in range(5):
        if secret[i] == guess[i]:
            comp[i] = 1
    seen = comp[:]
    for i in range(5):
        if not comp[i]:
            for j in range(5):
                if not seen[j] and secret[j] == guess[i]:
                    seen[j] = 1
                    comp[i] = 2
                    break
    return tuple(comp)


def reduce(words, hints):
    new_words = []
    for word in words:
        for hint in hints.keys():
            if hints[hint] != compare(word, hint):
                break
        else:
            new_words.append(word)
    return new_words


def reduce_count(words, hints):
    count = 0
    for word in words:
        for hint in hints.keys():
            if hints[hint] != compare(word, hint):
                break
        else:
            count += 1
    return count


def comparison_frequency(words, reference):
    frequency = {}
    for word in words:
        comp = compare(word, reference)
        if comp in frequency:
            frequency[comp] += 1
        else:
            frequency[comp] = 1
    return frequency


def list_bias(attempts, attempt_limit):
    if attempts != attempt_limit or attempt_limit == 0:
        return 0.5
    else:
        return 1


def best_average_reducer(words_a, words_g, bias=0.5):
    min_total_a = min_total_g = 1 + len(words_a)**2
    for candidate in words_a:
        total = 0
        comp_freq = comparison_frequency(words_a, candidate)
        for c in comp_freq.keys():
            total += comp_freq[c]**2
        if total < min_total_a:
            min_total_a = total
            best_candidate_a = candidate
            # print(candidate, total)
    best_candidate_g = 'aaaaa'
    for candidate in words_g:
        total = 0
        comp_freq = comparison_frequency(words_a, candidate)
        for c in comp_freq.keys():
            total += comp_freq[c]**2
        if total < min_total_g:
            min_total_g = total
            best_candidate_g = candidate
            # print(candidate, total)
    if bias*min_total_g < (1-bias)*min_total_a:
        return best_candidate_g
    else:
        return best_candidate_a


def max_reduce_solve(wordles_a, wordles_g, secret,
                     guesses={}, attempt_limit=0):
    attempts = len(guesses)
    while attempts != attempt_limit or attempt_limit <= 0:
        wordles_a = reduce(wordles_a, guesses)
        wordles_g = reduce(wordles_g, guesses)
        attempts += 1
        bias = list_bias(attempts, attempt_limit)
        new_guess = best_average_reducer(wordles_a, wordles_g, bias)
        guesses[new_guess] = compare(secret, new_guess)
        if guesses[new_guess] == [1, 1, 1, 1, 1]:
            return new_guess, guesses, True
    return new_guess, guesses, False


def max_reduce_solve_dic(wordles_a, wordles_g, secret, first_guess,
                         second_guesses, attempt_limit=0):
    attempts = 0
    guesses = {}
    while attempts != attempt_limit or attempt_limit <= 0:
        attempts += 1
        wordles_a = reduce(wordles_a, guesses)
        wordles_g = reduce(wordles_g, guesses)
        if attempts == 1:
            new_guess = first_guess
        elif attempts == 2:
            comp_str = ''.join(list(map(str, guesses[first_guess])))
            new_guess = second_guesses[comp_str]
        else:
            bias = list_bias(attempts, attempt_limit)
            new_guess = best_average_reducer(wordles_a, wordles_g, bias)
        guesses[new_guess] = compare(secret, new_guess)
        if guesses[new_guess] == [1, 1, 1, 1, 1]:
            return new_guess, guesses, True
    return new_guess, guesses, False
