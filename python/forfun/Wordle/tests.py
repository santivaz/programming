import json
from wordle_tools import compare, reduce_count

with open('wordlists/wordle-nyt-answers.json', 'r') as file:
    wordles_a = json.load(file)

with open('wordlists/wordle-nyt-guesses-no-answers.json', 'r') as file:
    wordles_g = json.load(file)

with open('wordlists/second_guesses_roate.json', 'r') as file:
    second_guesses = json.load(file)

first_guess = 'roate'

for word in wordles_a:
    guesses = {}
    guesses[first_guess] = compare(word, first_guess)
    print('FIRST: ', reduce_count(wordles_a, guesses))
    comp_str = ''.join(list(map(str, guesses[first_guess])))
    second_guess = second_guesses[comp_str]
    guesses[second_guess] = compare(word, second_guess)
    print('    SECOND: ', reduce_count(wordles_a, guesses))
