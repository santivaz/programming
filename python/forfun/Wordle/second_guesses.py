import json
from wordle_tools_V2 import best_average_reducer, reduce, compare

with open('wordlists/wordle-nyt-answers.json', 'r') as file:
    wordles_a = json.load(file)

with open('wordlists/wordle-nyt-guesses-no-answers.json', 'r') as file:
    wordles_g = json.load(file)

first_guess = 'roate'
second_guesses = {}

for word in wordles_a:
    c = compare(word, first_guess)
    new_a = reduce(wordles_a, {first_guess: c})
    new_g = reduce(wordles_g, {first_guess: c})
    if not (c in second_guesses):
        second_guesses[c] = best_average_reducer(new_a, new_g, 0.5)
        print(word, c, second_guesses[c])

with open('wordlists/second_guesses_tp_roate.json', 'w') as file:
    json.dump(second_guesses, file)
