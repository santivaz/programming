import json
from wordle_tools import max_reduce_solve_2_dic

with open('wordlists/wordle-nyt-answers.json', 'r') as file:
    wordles_a = json.load(file)

with open('wordlists/wordle-nyt-guesses-no-answers.json', 'r') as file:
    wordles_g = json.load(file)

with open('wordlists/second_guesses_roate.json', 'r') as file:
    second_guesses = json.load(file)

first_guess = 'roate'

total = 0
successes = 0
fails = []

for secret in wordles_a:
    print(secret)
    sol = max_reduce_solve_2_dic(wordles_a, wordles_g, secret, first_guess,
                                 second_guesses, 6)
    successes += sol[2]
    total += len(sol[1])
    print(len(sol[1]), sol[2], ' '.join(list(map(str, sol[1].keys()))))

    if not sol[2]:
        fails.append(secret)

num_a = len(wordles_a)
print('Average: ', total / num_a)
print('Successes: ', successes, '/', num_a, ' (', successes / num_a, ')')
print('Fails:', '\n'.join(list(map(str, fails))))

# Average:  3.595928973581637
# Successes:  2301 / 2309  ( 0.996535296665223 )
# Fails:
# joker
# patch
# sower
# vaunt
# waste
# watch
# wight
# willy
