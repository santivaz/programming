import json

SECRET = 'pasta'


def compare(secret, guess):
    hints = [0, 0, 0, 0, 0]
    for i in range(5):
        if secret[i] == guess[i]:
            hints[i] = 1
    seen = hints[:]
    for i in range(5):
        if not hints[i]:
            for j in range(5):
                if not seen[j] and secret[j] == guess[i]:
                    seen[j] = 1
                    hints[i] = 2
                    break
    return hints


def solve(wordles, guesses={}, secret=SECRET, attempt_limit=100):
    attempts = len(guesses)
    for word in wordles:
        for guess in guesses.keys():
            if guesses[guess] != compare(word, guess):
                break
        else:
            guesses[word] = compare(secret, word)
            attempts = attempts + 1
            if guesses[word] == [1, 1, 1, 1, 1]:
                # print(guesses)
                return word, guesses, True
            elif attempts == attempt_limit:
                return word, guesses, False


with open('wordles.json', 'r') as file:
    wordles = json.load(file)

# solution, guesses = solve(wordles, 'cozed')
# for key, value in guesses.items():
#     print(key, value)


first_guess = 'bread'
failures = 0
for wordle in wordles:
    if not solve(wordles, {first_guess: compare(wordle, first_guess)}, wordle, 6)[2]:
        print(wordle)
        failures = failures + 1

print('Failures: ', failures, 'out of ', len(wordles))
