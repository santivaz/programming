import json
from wordle_tools import best_average_reducer

wordlist = 'wordlists/twl06_5letter.json'

with open(wordlist, 'r') as file:
    wordles = json.load(file)

print('The best first guess for ' + wordlist
      + ' is ' + best_average_reducer(wordles))
