import json
from wordle_tools import reduce, best_average_reducer

with open('wordles.json', 'r') as file:
    wordles = json.load(file)

prev_guesses = {'lares': [0, 0, 0, 2, 0]}
                # 'pendu': [0, 1, 2, 0, 0]}
                # 'veiny': [0, 1, 1, 1, 0]}
                # 'meint': [0, 1, 1, 1, 0]}
candidates = reduce(wordles, prev_guesses)
print(candidates)
print('The best guess is: ', best_average_reducer(candidates))
