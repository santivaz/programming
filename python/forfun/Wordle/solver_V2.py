import json
from wordle_tools import compare, max_reduce_solve

with open('wordles.json', 'r') as file:
    wordles = json.load(file)

total = 0
max = 0
success = 0
word_index = 0
for secret in wordles:
    word_index += 1
    print(secret, '(', word_index, ')')
    # print('Guess: lares')
    initial = {'lares': compare(secret, 'lares')}
    result = max_reduce_solve(wordles, secret, initial)
    print(len(result[1]), ' '.join(list(map(str, result[1].keys()))))
    total += len(result[1])
    if len(result[1]) <= 6:
        success += 1
    if len(result[1]) > max:
        max = len(result[1])
        worst_word = secret
        print('NEW WORST: ', secret)
print('AVERAGE: ', total / len(wordles))
print('SUCCESS: ', success, ' OUT OF ', len(wordles),
      ' (', success / len(wordles), ')')
