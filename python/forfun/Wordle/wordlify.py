# import sys
import json

words = open('wordlists/wordle-nyt-allowed-guesses.txt')
word_list = words.readlines()
json_words = []
for i in range(len(word_list)):
    new_word = word_list[i].rstrip()
    if len(new_word) == 5:
        json_words.append(new_word)

print(len(json_words))

with open('wordlists/wordle-nyt-guesses-no-answers.json', 'w') as file:
    json.dump(json_words, file)
