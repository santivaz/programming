import webbrowser
from youtubesearchpython import VideosSearch

# Read songs from file
with open("songs.txt", "r") as file:
    songs = file.read().splitlines()

# Create an empty playlist
playlist = []

# Search for each song on YouTube and add the best match to the playlist
for song in songs:
    videosSearch = VideosSearch(song, limit=1)
    result = videosSearch.result()
    if result['result']:
        video_url = result['result'][0]['link']
        playlist.append((song, video_url))

# Open the playlist in a web browser
playlist_url = "https://www.youtube.com/watch_videos?video_ids=" + ','.join([v[1].split('=')[-1] for v in playlist])
webbrowser.open(playlist_url)

