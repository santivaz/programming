import tkinter as tk

root = tk.Tk()

root.title('MyWindow')
root.geometry('600x400+50+50')
root.resizable(False, False)

message = tk.Label(root, text="Hello World!")
message.pack()

root.mainloop()
