#!/bin/python

from os import system
# import numpy as np
import random


def clear():
    system("clear")


class Sudoku:
    def __init__(self, h=3, w=3):
        n = h*w
        self.grid = [[-1]*n for i in range(n)]
        self.size = n
        self.hight = h
        self.width = w

    def __str__(self):

        box_intersections = {
            "t": {
                "l": {
                    (1, 1): "┏"
                },
                "i": {
                    (1, 0): "┯",
                    (1, 1): "┳"
                },
                "r": {
                    (1, 1): "┓"
                }
            },
            "i": {
                "l": {
                    (0, 1): "┠",
                    (1, 1): "┣"
                },
                "i": {
                    (0, 0): "┼",
                    (1, 0): "┿",
                    (0, 1): "╂",
                    (1, 1): "╋"
                },
                "r": {
                    (0, 1): "┨",
                    (1, 1): "┫"
                }
            },
            "b": {
                "l": {
                    (1, 1): "┗"
                },
                "i": {
                    (1, 0): "┷",
                    (1, 1): "┻"
                },
                "r": {
                    (1, 1): "┛"
                }
            }
        }

        box_lines = {
            "v": {
                0: "│",
                1: "┃"
            },
            "h": {
                0: "─",
                1: "━"
            }
        }

        d = len(str(self.size))

        def num_str(x):
            sx = " " if x == -1 else str(1+x)
            dx = len(sx)
            return " "*(d-dx)+sx

        def row_str(i):
            row = ""
            for j in range(self.size):
                row = (
                    row
                    + box_lines["v"][j % self.width == 0]
                    + num_str(self.grid[i][j])
                )
            return row + box_lines["v"][1]

        def h_line(i):
            h_bold = (i % self.hight == 0)
            if i == 0:
                v_pos = "t"
            elif i == self.size:
                v_pos = "b"
            else:
                v_pos = "i"

            line = ""
            for j in range(self.size):
                h_pos = "l" if j == 0 else "i"
                line = (
                    line
                    + box_intersections[v_pos][h_pos][h_bold, j % self.width == 0]
                    + box_lines["h"][h_bold]*d
                )
            return line + box_intersections[v_pos]["r"][h_bold, 1]

        grid_str = ""
        for i in range(self.size):
            grid_str = (
                grid_str
                + h_line(i) + "\n"
                + row_str(i) + "\n"
            )
        return grid_str + h_line(self.size)

    def valid_values(self, x, y):
        valid = [True for i in range(self.size)]
        # check column
        for i in range(self.size):
            n = self.grid[i][y]
            if n >= 0 and i != x:
                valid[n] = False
        # check row
        for j in range(self.size):
            n = self.grid[x][j]
            if n >= 0 and j != y:
                valid[n] = False
        # get top left corner of local group
        a = x - (x % self.hight)
        b = y - (y % self.width)
        for i in range(self.hight):
            for j in range(self.width):
                n = self.grid[a+i][b+j]
                if n >= 0 and [x, y] != [a+i, b+j]:
                    valid[n] = False
        return valid

    def is_filled(self):
        for i in range(self.size):
            if -1 in self.grid[i]:
                return False
        return True

    def rand_change(self, x, y):
        try:
            self.grid[x][y] = random.choice(
                [i for i in range(self.size) if (
                    self.valid_values(x, y)[i] and i != self.grid[x][y]
                )]
            )
            return True
        except:
            return False


if __name__ == "__main__":
    h = 3
    w = 3
    n = h*w
    sdk = Sudoku(h, w)
    t = 0
    c = 0
    while not sdk.is_filled():
        x = random.randint(0, n-1)
        y = random.randint(0, n-1)
        if c == 40:
            sdk.grid[x][y] = -1
            c = c//2
        elif sdk.rand_change(x, y):
            c = 0
        else:
            c += 1
        t += 1
        clear()
        print(sdk)
        print(c)
        print(t)
