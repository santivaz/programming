import pandas as pd
import numpy as np

from glob import glob

import cv2
import matplotlib.pylab as plt


def get_pixe_distribution():
    pd.Series(img_mpl.flatten()).plot(kind='hist', bins=50, title='Distribution of pixel values')
    plt.show()


def show_image():
    fig, ax = plt.subplots(figsize=(10,10))
    ax.imshow(img_mpl)
    ax.axis('off')
    plt.show()


def show_color_channels():
    fig, axs = plt.subplots(1, 3, figsize=(15, 5))
    axs[0].imshow(img_mpl[:,:,0], cmap='Reds')
    axs[1].imshow(img_mpl[:,:,1], cmap='Greens')
    axs[2].imshow(img_mpl[:,:,2], cmap='Blues')
    axs[0].axis('off')
    axs[1].axis('off')
    axs[2].axis('off')
    axs[0].set_title('Red channel')
    axs[1].set_title('Green channel')
    axs[2].set_title('Blue channel')
    plt.show()

if __name__ == '__main__':
    dog_files = glob('media/training_set/dogs/*.jpg')
    cat_files = glob('media/training_set/cats/*.jpg')

    img_mpl = plt.imread(cat_files[2911]) # load RGB
    img_cv2 = cv2.imread(cat_files[2911]) # load BGR

    show_color_channels()
