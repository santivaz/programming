#!/usr/bin/python

import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify

Notify.init("test")
Notify.Notification.new('Cron test', 'Hello world!').show()
