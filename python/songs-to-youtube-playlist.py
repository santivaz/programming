from youtubesearchpython import VideosSearch
import webbrowser

# List of songs
songs = [
    "Johannes Brahms - Symphony No. 4 in E minor, Op. 98",
    "Pyotr Ilyich Tchaikovsky - Symphony No. 6 in B minor, Op. 74 'Pathétique'",
    "Antonín Dvořák - Symphony No. 9 in E minor, Op. 95 'From the New World'",
    "Gustav Mahler - Symphony No. 5 in C-sharp minor",
    "Richard Strauss - Also sprach Zarathustra, Op. 30",
    "Edvard Grieg - Piano Concerto in A minor, Op. 16",
    "Sergei Rachmaninoff - Piano Concerto No. 2 in C minor, Op. 18",
    "Franz Liszt - Piano Sonata in B minor, S. 178",
    "Jean Sibelius - Symphony No. 2 in D major, Op. 43",
    "Claude Debussy - Prélude à l'après-midi d'un faune"
]

# Create an empty playlist
playlist = []

# Search for each song on YouTube and add the best match to the playlist
for song in songs:
    videosSearch = VideosSearch(song, limit=1)
    result = videosSearch.result()
    if result['result']:
        video_url = result['result'][0]['link']
        playlist.append((song, video_url))

# Open the playlist in a web browser
playlist_url = "https://www.youtube.com/watch_videos?video_ids=" + ','.join([v[1].split('=')[-1] for v in playlist])
webbrowser.open(playlist_url)

