/* 
 * File:   main.cpp
 * Author: Santiago
 *
 * Created on 2 de abril de 2018, 22:54
 *
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h> //printf,scanf, puts NULL
#include <stdlib.h> //srand, ramd
#include <time.h> //time


using namespace std;

class Buscaminas {
private:
    int dimx; //dimensión vertical del campo de minas.(<1000)
    int dimy; //Dimensión horizontal del campo de minas. (<100)
    int minas; //Número de minas en el campo.

    //Será true mientras no se toque una bomba.
    bool sobrevive = true;

    //Será falso mientras o bien sólo quedan ocultas las bombas o bien están marcadas exáctamente las casillas con bomba.
    bool victoria = false;

    //{{fila}{columna}{bomba(-1) o número de bombas cerca, casilla oculta (0)  casilla visible (1) casilla oculta marcada (2)}} 
    //El motivo del 999 y el 99 es porque estos son los valores máximos de dimx y dimy.
    int campo[999][99][2];

    //{número de descubiertas, número de casillas marcadas, número de minas marcadas.}
    int estado[3] = {0, 0, 0};


public:
    void setdimx(int);
    void setdimy(int);
    void setAll(int, int, int);
    void setminas(int);
    void setCampoCero(); //Todos los valores de campo se hacen iguales a 0. Esto debería ser así por defecto, pero para arrays grandes no siempre resulta ser cierto.
    void minar(); //Coloca las minas en posición aleatoria.
    void numerar(); //Cuenta las minas cerca de cada casilla.
    void impCampo(); //Muestra el campo por consola.
    void impCampoCoord(); //Muestra el campo por consola añadiéndole coordenadas
    void prepCampo(); //Pone el campo a cero, mina y numera.
    void descubrir(int, int); //Equivale a clicar sobre una casilla.
    void mostMinas(); //Pone visibles todas las minas no marcadas y desmarca las casillas mal marcadas.
    void marcar(int, int); //Marca o desmarca una casilla.
    void getEstado(); //Pensada únicamente para debugging. Muestra el estado de las variables que controlan el fin del juego.

    bool seguir() {
        return (sobrevive && !victoria);
    } //Determina si el juego ha terminado.
    void getMensaje();

    int getRestantes() {
        return minas - estado[1];
    }
};

void Buscaminas::setdimx(int dimx) {
    this -> dimx = dimx;
}

void Buscaminas::setdimy(int dimy) {
    this -> dimx = dimy;
}

void Buscaminas::setAll(int dimx, int dimy, int minas) {
    this -> dimx = dimx;
    this -> dimy = dimy;
    this -> minas = minas % (dimy * dimx); //evita que minas sea demasiado grande y por lo tanto la operación minar no sea infinita.
}

void Buscaminas::setminas(int minas) {
    this -> minas = minas;
}

void Buscaminas::minar() {
    int minasPuestas = 0;
    int posx, posy;
    srand(time(NULL));
    do {
        //obtenemos una posición aleatoria.
        posx = rand() % dimx;
        posy = rand() % dimy;

        //Si la posición está libre colocamos una mina.
        if (0 == campo[posx][posy][0]) {
            campo[posx][posy][0] = -1;
            minasPuestas++;
        }
    } while (minasPuestas < minas); //Repetimos si quedan minas por poner.

}

void Buscaminas::numerar() {
    //Recorremos todas las casillas
    for (int x = 0; x < dimx; x++) {
        for (int y = 0; y < dimy; y++) {

            //Nos movemos al rededor de la casilla.
            for (int i = x - 1; i < x + 2; i++) {
                for (int j = y - 1; j < y + 2; j++) {

                    //Si la casilla no está fuera del campo, si tiene una bomba y si la central no tiene una bomba. Añadimos uno al numero de bombas cercanas.
                    if (i>-1 && i < dimx && j>-1 && j < dimy && campo[i][j][0] == -1 && campo[x][y][0] != -1) ++campo[x][y][0];
                }
            }
        }
    }
}

void Buscaminas::impCampo() {
    for (int x = 0; x < dimx; x++) {
        for (int y = 0; y < dimy; y++) {
            if (campo[x][y][0] > 0 && campo[x][y][1] == 1) cout << campo[x][y][0] << " "; //Número descubierto.
            if (campo[x][y][0] == 0 && campo[x][y][1] == 1) cout << "  "; //Vacía descubierta.
            if (campo[x][y][0] == -1 && campo[x][y][1] == 1) cout << "X "; //Bomba descubierta.
            if (campo[x][y][1] == 0) cout << "- "; //Oculta.
            if (campo[x][y][1] == 2) cout << "# "; //Marcada.

        }
        cout << endl;
    }
}

void Buscaminas::impCampoCoord() {
    //Las casillas tienen una anchura de tres caracteres.
    //Si cambiamos el tamaño máximo hay que tener cuidado con el número de espacios.

    //Coordenada y.
    cout << "   |";
    for (int i = 1; i <= dimy; i++) {
        if (i < 10) cout << " ";
        cout << i << "|";
    }
    cout << endl;

    for (int x = 0; x < dimx; x++) {
        //coordenada x.
        cout << x + 1;
        if (x < 9) cout << " ";
        if (x < 99) cout << " ";
        cout << "| ";

        for (int y = 0; y < dimy; y++) {
            if (campo[x][y][0] > 0 && campo[x][y][1] == 1) cout << campo[x][y][0] << "  "; //Número decubierto.
            if (campo[x][y][0] == 0 && campo[x][y][1] == 1) cout << "   "; //Vacía descubierta.
            if (campo[x][y][0] == -1 && campo[x][y][1] == 1) cout << "X  "; //Bomba descubierta.
            if (campo[x][y][1] == 0) cout << "-  "; //Oculta.
            if (campo[x][y][1] == 2) cout << "#  "; //Marcada.
        }
        cout << endl;
    }
}

void Buscaminas::setCampoCero() {
    for (int x = 0; x < dimx; x++) {
        for (int y = 0; y < dimy; y++) {
            campo[x][y][0] = 0;
            campo[x][y][1] = 0;
        }
    }
}

void Buscaminas::prepCampo() {
    //setCampoCero()
    for (int x = 0; x < dimx; x++) {
        for (int y = 0; y < dimy; y++) {
            campo[x][y][0] = 0;
            campo[x][y][1] = 0;
        }
    }

    //minar()
    int minasPuestas = 0;
    int posx, posy;
    srand(time(NULL));
    do {
        posx = rand() % this -> dimx;
        posy = rand() % this -> dimy;
        if (0 == campo[posx][posy][0]) {
            campo[posx][posy][0] = -1;
            minasPuestas++;
        }
    } while (minasPuestas < this -> minas);

    //numerar()   
    for (int x = 0; x < this -> dimx; x++) {
        for (int y = 0; y < this -> dimy; y++) {
            for (int i = x - 1; i < x + 2; i++) {
                for (int j = y - 1; j < y + 2; j++) {
                    if (i>-1 && i < this -> dimx && j>-1 && j < this -> dimy && campo[i][j][0] == -1 && campo[x][y][0] != -1) ++campo[x][y][0];
                }
            }
        }
    }
}

void Buscaminas::descubrir(int posx, int posy) {
    if (campo[posx][posy][0] == -1 && campo[posx][posy][1] == 0) {
        sobrevive = false;
    } else if (campo[posx][posy][1] == 0 && campo[posx][posy][1] != -1) {
        campo[posx][posy][1] = 1;
        estado[0]++;
    }

    bool cambios;
    do {
        cambios = false;
        for (int x = 0; x < dimx; x++) {
            for (int y = 0; y < dimy; y++) {
                if (campo[x][y][1] == 0 && campo[x][y][0] != -1) {
                    for (int i = x - 1; i < x + 2; i++) {
                        for (int j = y - 1; j < y + 2; j++) {
                            if (i>-1 && i < this -> dimx && j>-1 && j < this -> dimy && !(i == x && j == y) && campo[i][j][1] == 1 && campo[i][j][0] == 0) {
                                cambios = true;
                                campo[x][y][1] = 1;
                            }
                        }
                    }
                    if (campo[x][y][1] == 1) estado[0]++;
                }
            }
        }
    } while (cambios);

    if (estado[0] == dimx * dimy - minas) victoria = true;
}

void Buscaminas::mostMinas() {
    for (int x = 0; x < dimx; x++) {
        for (int y = 0; y < dimy; y++) {
            if (campo[x][y][0] == -1 && campo[x][y][1] == 0) {
                campo[x][y][1] = 1;
            } else if (campo[x][y][0] != -1 && campo[x][y][1] == 2) {
                campo[x][y][1] = 0;
            }
        }
    }
}

void Buscaminas::marcar(int posx, int posy) {
    if (campo[posx][posy][1] == 0) {
        campo[posx][posy][1] = 2;
        estado[1]++;
        if (campo[posx][posy][0] == -1) estado[2]++;
    } else if (campo[posx][posy][1] == 2) {
        campo[posx][posy][1] = 0;
        estado[1]--;
        if (campo[posx][posy][0] == -1) estado[2]--;
    }

    if (estado[2] == minas && estado[1] == minas) victoria = true;
}

void Buscaminas::getEstado() {
    cout << "sobrevive: " << sobrevive << endl;
    cout << "victoria: " << victoria << endl;
    cout << "estado[0]: " << estado[0] << endl;
    cout << "estado[1]: " << estado[1] << endl;
    cout << "estado[2]: " << estado[2] << endl;
}

void Buscaminas::getMensaje() {
    if (victoria) cout << "¡¡HAS GANADO, ENHORABUENA!!\n";
    if (!sobrevive) cout << "¡¡VAYA, HAS PERDIDO!!\n";
}

void jugarBuscaminas(int dimx, int dimy, int minas) {
    int x, y;
    string sx;
    Buscaminas partida;
    partida.setAll(dimx, dimy, minas);
    partida.prepCampo();
    bool marcar = false;
    do {
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
        partida.impCampoCoord();
        cout << "Minas restantes: " << partida.getRestantes() << ".\n";
        cout << "Escriba las coordenadas (para cambiar a/de modo marcar escribe m):\n";
        if (marcar) cout << "[marcar] ";
        cin >> sx;
        if (sx == "m") {
            marcar = !marcar;
        } else if (!marcar) {
            x = atoi(sx.c_str()) - 1;
            cin >> y;
            y -= 1;
            partida.descubrir(x, y);
        } else if (marcar) {
            x = atoi(sx.c_str()) - 1;
            cin >> y;
            y -= 1;
            partida.marcar(x, y);
        }

    } while (partida.seguir());

    partida.mostMinas();
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    partida.impCampoCoord();
    partida.getMensaje();

}

int main() {
    string dificultad;
    cout << "Escoge la dificultad (1-3,P): ";
    cin >> dificultad;
    if (dificultad == "P" || dificultad == "p") {
        int dimx, dimy, minas;
        cout << "Indica las medidas(vertical<1000 horizontal<100): ";
        cin >> dimx;
        cin >> dimy;
        cout << "Indica el número de bombas: ";
        cin >> minas;
        jugarBuscaminas(dimx, dimy, minas);
    } else {
        switch (atoi(dificultad.c_str())) {
            case 1: jugarBuscaminas(8, 8, 10);
                break;
            case 2: jugarBuscaminas(16, 16, 40);
                break;
            case 3: jugarBuscaminas(16, 30, 99);
                break;
        }
    }

    cout << "Presiona ENTER para salir...";
    cin.ignore();
    cin.ignore();
}
