clear;
digits(115);
a = vpa(0);
n=100;
%n+1 roots
for i = 1:n
    a=vpa(2)^(i+2)*sqrt(vpa(2)^(2*i+1)+a);
end

p = sqrt(vpa(2)^(2*n+3)-a)
d = vpa(pi)-p
dgts = -floor(double(log(abs(d))/log(vpa(10))))