

function klein(R,r)
t = 0:pi/50:2*pi;
n= 1/2;
l = -1:1/10:1;
x = @(i,j) cos(i)*(R +r*j*cos(n*i));
y = @(i,j) sin(i)*(R +r*j*cos(n*i));
z = @(i,j) r*j*sin(n*i);
T = size(t);
L = size(l);
for i= 1:1:T(2);
    for j= 1:1:L(2);
        X(i,j)=x(t(i),l(j));
        Y(i,j)=y(t(i),l(j));
        Z(i,j)=z(t(i),l(j));
    end
end

mesh(X,Y,Z)

function mobius(R,r)
t = 0:pi/50:2*pi;
n= 1/2;
l = -1:1/10:1;
x = @(i,j) cos(i)*(R +r*j*cos(n*i));
y = @(i,j) sin(i)*(R +r*j*cos(n*i));
z = @(i,j) r*j*sin(n*i);
T = size(t);
L = size(l);
for i= 1:1:T(2);
    for j= 1:1:L(2);
        X(i,j)=x(t(i),l(j));
        Y(i,j)=y(t(i),l(j));
        Z(i,j)=z(t(i),l(j));
    end
end

mesh(X,Y,Z)

end