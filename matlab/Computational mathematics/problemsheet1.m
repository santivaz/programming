%% Problem Sheet 1
%
%% Creating and plotting a function
%
% I will begin by creating the function $f(x) = e^{sin(x^2)}$:
f = @(x) exp(sin(x.^2));

%% 
%
% Now that we have our function, we can see which values it returns when we plug in different values for $x$:
f(0)
f(sqrt(pi))
f(1.473)

%%
% And finally, let's plot the function:

%Plots the function on the interval [0, 2*pi]
fplot (f, [0 2*pi],'LineWidth', 2)

%Title, axis labels and size
hold on;
title('Santiago Vazquez Saez, Lincoln College');
axis([0 2*pi 0 3])
xlabel('x');
ylabel('f(x)');

%Some other plots with evenly spaced points
x10 = linspace(0, 2*pi, 10);
x20 = linspace(0, 2*pi, 20);
x500 = linspace(0, 2*pi, 500);
plot(x10, f(x10),'g--')
plot(x20, f(x20),'r:','LineWidth',1.5)
plot(x500, f(x500), 'kx','MarkerSize',6,'LineWidth',0.75)

%Adds a legend to our curves
legend('f(x) = e^{sin(x^2)}','10 data points', ...
    '20 data points','500 data points')

%% Precision of floating-points
% 
% MATLAB stores approximations of numbers, this means that it is not infinitely accurate and therefore we can get some unsatisfactory results if we are not careful. An example of this is is the following: we know that $$ 2 = \left(\sqrt 2 \right)^2 = \left( \left( \sqrt{\sqrt{2}}\right)^2\right)^2 = \dots =  \left( \left(\dots\left( \sqrt{\sqrt{\dots \sqrt{2}\dots}}\right)^2\dots\right)^2\right)^2,$$ but  if we calculate these numbers with MATLAB, we will see that there is a small difference with its real value, and this difference increases as we increase the number of iterations. The code below shows exactly what we have just mentioned. The output shows how far the values are from $2$. The last number corresponds to the 50th iteration:
for n=1:5:50

    x=2;
    y=x;
    for i=1:n

        y=sqrt(y);

    end
    for i=1:n

        y=y.^2;

    end
    disp(abs(y-x));

end
