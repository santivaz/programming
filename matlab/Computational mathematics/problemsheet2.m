%% Sheet 2, Santiago Vázquez Sáez
%% Question 1

%Create the functions
f = @(x)(5*x.^2 + 3*x - 4);
g = @(x)(-4*x.^2 + 6*x +5);

%Plot thhe functions
fplot(f, [-2 2])
hold on
fplot(g, [-2 2])

%Solve the intersection points.
syms x;
intrs = double(solve(f(x)-g(x) == 0, x));

%Find the area bounded by the two courves.
a = abs(double(int(f(x)-g(x), x, intrs)));

%Print the answer
fprintf('\n(b)The functions f and g intersect at points (%f, %f) and (%f, %f).\n\n(c)The area bounded by them is %f.\n', ...
    intrs(1), f(intrs(1)), intrs(2), f(intrs(2)), a)

%% Question 2

%Approximation of the area using 10 gridpoints
X = linspace(intrs(1),intrs(2),10);
Q10 = trapz(X,abs(f(X)-g(X)));
fprintf('\n(a)The numerical approximation of the area using 10 gridpoints is %f.\n', Q10)

%Approximation of the area using 100 gridpoints
Y = linspace(intrs(1),intrs(2),100);
Q100 = trapz(Y,abs(f(Y)-g(Y)));
fprintf('\n(b)The numerical approximation of the area using 100 gridpoints is %f.\n', Q100)

%% Question 3

%Approximation of the area using 10 gridpoints with myTrapz
myQ10 = myTrapz(X,abs(f(X)-g(X)));
fprintf('\n(a)The numerical approximation of the area using 10 gridpoints and myTrapz is %f.\n', myQ10)

%Approximation of the area using 100 gridpoints with myTrapz
myQ100 = myTrapz(Y,abs(f(Y)-g(Y)));
fprintf('\n(b)The numerical approximation of the area using 100 gridpoints and myTrapz is %f.\n', myQ100)


function Q = myTrapz(X, Y)

%Checks for errors in imput.
if ~isvector(X) || ~isvector(Y) || length(X)~=length(Y)
    error('Inputs must be vectors of the same size.')
end

%Gets result by adding trapezoids
Q = 0;
for i = 2:length(X)
    Q = Q + 0.5*(X(i)-X(i-1))*(Y(i)+Y(i-1));
end

end