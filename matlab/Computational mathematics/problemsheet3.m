%% Sheet 3, Santiago Váquez Sáez

%% Question 1
syms u(t);
eqn = (t+1)^2*diff(u, t, 2) - 3*(t+1)*diff(u,t) + t == 1;
dy(t) = diff(u,t);
conds = [u(0) == 1, dy(0) == 1];
u(t) = dsolve(eqn,conds)

%% Question 2
% By making the substitution, we have
% 
% <<formula1.png>>
%
% Hence the original equation becomes
%
% <<formula2.png>>

%% Question 3
%
% Using "ode_template.m" we can create a program to numerically solve the
% system from Question 2 in the interval [0,1]:

%Initial conditions
y0=[1;1];
%Limits
limits=[0,1];
%Matrix with the solutions of my_system, which is defined at the end
[t,sol] = ode45(@my_system,limits,y0);

%% Question 4

plot(t,sol(:,1))
hold on;
fplot(u,[0 1])
legend("Numerical aproximation of u(t)","u(t)");
title('Santiago Vazquez Saez, Lincoln College');
%%
%The function containing the system from Q2
function dU=my_system(t, U)
dU=zeros(2,1);
dU(1)= U(2);
dU(2)= (1-t+3*(t+1)*U(2))/(t+1)^2;
end