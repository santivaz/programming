function [t,sol]=ode_template() % Function name must be the same as filename.
y0=???;                  % Specify initial conditions.
limits=???;             % Input limits of integration. 
[t,sol] = ode45(???); % Command to numerically solve the system 'my_system'
end

function dU=my_system(???) % Name of the system we wish to solve

% dU is an nx1 vector, where n is the number of equations in the system.
dU=???;       

% Input equations below

dU(1)=???
dU(2)=???
end